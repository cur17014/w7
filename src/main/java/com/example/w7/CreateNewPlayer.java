package com.example.w7;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.regex.Pattern;

@WebServlet(name = "createNewPlayer", value = "/createNewPlayer")
public class CreateNewPlayer extends HttpServlet {
    private String playerName;
    private String playerRank;
    private String playerFriendsList;

    /*public void init() {
        message = "Hello World!";
    }*/

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        playerName = request.getParameter("pName");
        playerRank = request.getParameter("rank");
        playerFriendsList = request.getParameter("fList");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String result = createNewPlayer(out);

        /*request.*/
        // Hello
        out.println("<html><body>");
        out.println("<h1>" + result + "</h1>");
        out.println("</body></html>");
    }

    private String createNewPlayer(PrintWriter out) {
        boolean playerCreateStatus = true;
        boolean useRank = false;
        boolean useFL = false;

        try {
            if (playerName == null || playerName == "") {
                playerCreateStatus = false;
                throw new NullPointerException("Unable to create a player with no name");
            }

            int pRank = -1;
            //ensure that the rank is an int
            if(playerRank != null && playerRank != "none" && playerRank != "empty" && playerRank != "") {
                if (!Pattern.compile("[0-9]*").matcher(playerRank).matches()) {
                    playerCreateStatus = false;
                    throw new IllegalArgumentException("Cannot create a new player with no rank " + playerRank);
                }
                if (Integer.parseInt(playerRank) <= 0) {
                    playerCreateStatus = false;
                    throw new IllegalArgumentException("Cannot create a new player with rank " + playerRank);
                } else {
                    useRank = true;
                    pRank = Integer.parseInt(playerRank);
                }
            }

            if(playerFriendsList != null || playerFriendsList != "empty"){
                useFL = true;
            }

            if(!useRank){
                Player np = new Player(playerName);
                playerCreateStatus = true;
            }
            else{
                if(!useFL){
                    Player np = new Player(playerName, pRank);
                    playerCreateStatus = true;

                }
                else{
                    String[] parsedFL = playerFriendsList.split(",");
                    Player np = new Player(playerName, pRank, parsedFL);
                    playerCreateStatus = true;
                }
            }


        }catch(Exception e){
            out.println("<html><body>");
            out.println("<h1> Exception thrown: " + e + "</h1>");
            out.println("</body></html>");
        }

        if(!playerCreateStatus){
            return "There was an error when creating the new player!!";
        }
        else{
            return "Created a new player with no issues!! :)";
        }
    }

    public void destroy() {
    }
}