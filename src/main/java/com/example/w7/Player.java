package com.example.w7;

import java.util.PriorityQueue;
import java.util.Queue;

public class Player {
	
	private String name;
	private int rank;
	private String[] friendsList;
	private Queue<String> downloadQueue;
    private int friendsListSize = 0;

	public Player(String name, int rank, String[] friendsList) {
		this.name = name;
		this.rank = rank;
		this.friendsList = friendsList;
        this.friendsListSize = friendsList.length;
		downloadQueue = new PriorityQueue<String>();
	}

    public Player(String name, int rank){
        this.name = name;
        this.rank = rank;
        this.friendsList = new String[10];
        downloadQueue = new PriorityQueue<String>();
    }

    public Player(String name){
        this.name = name;
        this.rank = friendsListSize + 1;
        this.friendsList = new String[10];
        downloadQueue = new PriorityQueue<String>();
    }

	// Name
    public String getName() { return name; }
    public void setName(String name) {
        if(name != null && !name.equalsIgnoreCase(" ")) {
            this.name = name;
        }
        else {
            throw new NullPointerException();
        }
    }
    
    // Rank
    public int getRank() {
        return rank;
    }
    public void setRank(int rank) {
        this.rank = rank;
    }
    
    // FriendsList
    public String[] getFriendsList() {
        return friendsList;
    }
    public void setFriendsList(String[] friendsList) {
        if(friendsList != null) {
            this.friendsList = friendsList;
        }
        else{
            throw new NullPointerException();
        }
    }
    
	// Download Queue
    public Queue<String> getDownloads() {
        return downloadQueue;
    }
    public void setDownloadQueue(Queue<String> downloadQueue) {
        if(downloadQueue != null) {
            this.downloadQueue = downloadQueue;
        }
        else{
            throw new NullPointerException();
        }
    }
    
    // Add a single item to player queue
    // Returns true if player was added false otherwise
    // throws an error if a null or empty value is given.
    public boolean addToFriendsList(String newFriend) {
    	if(newFriend != null && newFriend != " ") {

            // Check if friend is already in list
            for(int i = 0; i < friendsListSize; i++){
                if(friendsList[i].equalsIgnoreCase(newFriend)){
                    return false;
                }
            }
            if(friendsList.length != friendsListSize) {
                friendsList[friendsListSize] = newFriend;
                friendsListSize++;
                return true;
            }
            else{
                expandFriendsList();
                friendsList[friendsListSize] = newFriend;
                friendsListSize++;
                return true;
            }
    	}
        else{
            throw new NullPointerException();
        }
    }

    private void expandFriendsList() {
        String[] resizedFriendsList = new String[friendsListSize * 3];
        for(int i = 0; i < friendsListSize; i++){
            resizedFriendsList[i] = friendsList[i];
        }
        friendsList = resizedFriendsList;
    }

    public boolean removeFromFriendsList(String friend){
        if(friend.length() != 0 && !friend.equals(null)){
            for(int i = 0; i < friendsListSize; i++){
                if(friendsList[i].equalsIgnoreCase(friend)){
                    int itemIndexToRemove = i;
                    boolean removed = false;
                    String[] newFriendsList = new String[friendsList.length];
                    for(int j = 0; j < friendsListSize; j++){
                        if(j == itemIndexToRemove && !removed){
                            removed = true;
                            if( j != friendsList.length - 1){
                                newFriendsList[j] = friendsList[j+1];
                                i++;
                                friendsListSize--;
                            }
                        }
                        else
                        {
                            newFriendsList[j] = friendsList[j];

                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /* Add a single item to player queue
    * Returns true if player was added
    */
    public boolean addToDownloads(String newDownload) {
        if (newDownload != null && newDownload != " ") {
            if (downloadQueue != null) {
                downloadQueue.add(newDownload);
                return true;
            }
        }
        return false;
    }
}
