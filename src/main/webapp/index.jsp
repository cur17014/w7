<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>New Player Creator</title>
</head>
<body>
<h1><%= "Hello! Create A new Player here!" %>
</h1>
<br/>
<form method="post" action="createNewPlayer">
    <label for="pName">Name:</label><br>
    <input type="text" id="pName" name="pName" value=""><br>
    <label for="rank">Rank:</label><br>
    <input type="text" id="rank" name="rank" value=""><br><br>
    <label for="fList">Friends to add to Friends List:</label><br>
    <input type="text" id="fList" name="fList" value=""><br><br>
    <input type="submit" value="Submit">
</form>
</body>
</html>